
Run = true;		-- Флаг поддержания работы скрипта

function main()
	-- Цикл, поддерживающий работу скрипта
	while Run do writeDeal(); end;
end;

-- Вызывается терминалом QUIK в момент остановки скрипта
function OnStop()
	-- Выключает флаг, чтобы остановить цикл while внутри main
	Run = false;

end;

function getInstrName(tableName, sec)
	for i = 0,getNumberOf(tableName) - 1 do
	   -- ЕСЛИ строка по нужному инструменту И чистая позиция не равна нулю ТО
	   if getItem(tableName,i).sec_code == sec then
			return getItem(tableName,i).name
	   end;
	end;
end;

function writeDeal()
	local rows = readTable();
	JSON = io.open(getScriptPath().. "/export/" .. tostring(os.date("%d.%m.%Y")) .. ".json", "w+");
	JSON:write(rows);
	JSON:close();	
	sleep(60000);
end;

function CheckBit(flags, bit)
   -- Проверяет, что переданные аргументы являются числами
   if type(flags) ~= "number" then error("Ошибка!!! Checkbit: 1-й аргумент не число!"); end;
   if type(bit) ~= "number" then error("Ошибка!!! Checkbit: 2-й аргумент не число!"); end;
   local RevBitsStr  = ""; -- Перевернутое (задом наперед) строковое представление двоичного представления переданного десятичного числа (flags)
   local Fmod = 0; -- Остаток от деления 
   local Go = true; -- Флаг работы цикла
   while Go do
      Fmod = math.fmod(flags, 2); -- Остаток от деления
      flags = math.floor(flags/2); -- Оставляет для следующей итерации цикла только целую часть от деления           
      RevBitsStr = RevBitsStr ..tostring(Fmod); -- Добавляет справа остаток от деления
      if flags == 0 then Go = false; end; -- Если был последний бит, завершает цикл
   end;
   -- Возвращает значение бита
   local Result = RevBitsStr :sub(bit+1,bit+1);
   if Result == "0" then return 0;     
   elseif Result == "1" then return 1;
   else return nil;
   end;
end;

function readTable()
	local rows = "[";
	local Operation = "";
	for i = 0,getNumberOf("trades") - 1 do
		if CheckBit(getItem("trades",i).flags, 2) == 1 then Operation = "SELL"; else Operation = "BUY"; end;
		rows = rows .. "[" .. 
			'"' .. tostring(getItem("trades",i).datetime.year) .. "." .. tostring(getItem("trades",i).datetime.month) .. "." .. tostring(getItem("trades",i).datetime.day)   .. '"' .. "," ..
			'"' .. tostring(getItem("trades",i).datetime.hour) .. ":" .. tostring(getItem("trades",i).datetime.min) .. ":" .. tostring(getItem("trades",i).datetime.sec)   .. '"' .. "," ..
			'"' .. Operation   .. '"' .. "," ..
			'"' .. tostring(getItem("trades",i).price) .. '"' .. "," ..
			'"' .. tostring(getItem("trades",i).qty) .. '"' .. "," .. 
			'"' .. tostring(getItem("trades",i).clearing_comission) .. '"' .. "," ..
			'"' .. tostring(getItem("trades",i).exchange_comission / 2) .. '"' .. "," ..
			'"' .. tostring(getItem("trades",i).tech_center_comission) .. '"' .. "," ..
			'"' .. tostring(getItem("trades",i).sec_code) .. '"' .. "," ..
			'"' .. getInstrName('securities', tostring(getItem("trades",i).sec_code)) .. '"';
		if i == getNumberOf("trades") - 1 then
			rows = rows .. "]";
		else
			rows = rows .. "],";
		end;
	end;
	rows = rows .. "]"

	return rows;
end;
